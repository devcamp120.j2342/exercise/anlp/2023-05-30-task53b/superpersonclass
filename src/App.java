import models.Person;
import models.Staff;
import models.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("an", "hcm");
        Person person2 = new Person("minh", "qn");
        System.out.println(person1);
        System.out.println(person2);

        Student student1 = new Student("an", "hcm", "a", 1, 10000);
        Student student2 = new Student("minh", "qn", "a", 1, 10000);
        System.out.println(student1);
        System.out.println(student2);

        Staff staff1 = new Staff("vu", "hcm", "bk", 222000);
        Staff staff2 = new Staff("lau", "hcm", "bk", 222000);
        System.out.println(staff1);
        System.out.println(staff2);
    }
}
